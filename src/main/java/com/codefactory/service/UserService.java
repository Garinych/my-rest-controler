package com.codefactory.service;

import com.codefactory.myrestfulproject.model.User;
import com.codefactory.myrestfulproject.model.UserResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class UserService {

    Logger logger = LoggerFactory.getLogger(UserService.class);
    private static final String USER_SEARCH_URL="https://bitbucket.org/Garinych/my-rest-controler/raw/08578cd28c03ef5cf017aadaac3ba1faa8ec06d4/src/main/resources/user.json/";

    private static final String USER_POST_URL="https://postman-echo.com/post";

    @Autowired
    RestTemplate restTemplate;


    public List<User> getUserByEmail(String email) {
        return restTemplate.getForEntity(USER_SEARCH_URL+email, UserResult.class).getBody().getResult();
    }

    public List<User> getUserById(Long id) {
        ResponseEntity<UserResult> responseEntity = restTemplate.getForEntity(USER_SEARCH_URL+id, UserResult.class);
        return  responseEntity.getBody().getResult();
    }

    public User addUser(User user) {
        return restTemplate.postForEntity(USER_POST_URL, user, User.class).getBody();
    }
}
