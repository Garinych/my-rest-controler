package com.codefactory.myrestfulproject.repository;

import com.codefactory.myrestfulproject.model.User;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository
public class UserRepository  {

    private static final Map<Long, User> userMap = new HashMap<Long, User>();

    static {
        initUsers();
    }

    private static void initUsers() {
        User user1 = new User((long)1, "Smith", "Clerk", "sc@mail.ru");
        User user2 = new User((long)2, "Allen", "Salesman","as@gmail.com");
        User user3 = new User((long)3, "Jones", "Manager","jm@mail.es");

        userMap.put(user1.getId(), user1);
        userMap.put(user2.getId(), user2);
        userMap.put(user3.getId(), user3);
    }

    public User getUser(Long id) {
        return userMap.get(id);
    }

    public User addUser(User user) {
        userMap.put(user.getId(), user);
        return user;
    }

    public User updateUser(User user) {
        userMap.put(user.getId(), user);
        return user;
    }

    public void deleteUser(Long id) {
        userMap.remove(id);
    }

    public List<User> getAllUsers() {
        Collection<User> c = userMap.values();
        List<User> list = new ArrayList<User>();
        list.addAll(c);
        return list;
    }


}
