package com.codefactory.myrestfulproject.controller;


import com.codefactory.myrestfulproject.model.User;
import com.codefactory.myrestfulproject.repository.UserRepository;
import com.codefactory.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserRestController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Welcome to RestTemplate";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<User> getAllUsers (){
        List<User> list = (List<User>) userRepository.getAllUsers();
        return list;
    }


    /*@RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public User getUser(@PathVariable("id") Long id){
        return userRepository.getUser(id);
    }*/

    //RestTemplate
    @RequestMapping(value = "/users/{email}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody List<User> getUserByEmail(@PathVariable("email") String email){
        return userService.getUserByEmail(email);
    }

    //RestTemplate
    @RequestMapping(value = "/sync/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody List<User> getUserById(@PathVariable("id") Long id){
        return userService.getUserById(id);
    }


    //RestTemplate
    @RequestMapping(value = "https://postman-echo.com/post", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody User addNewUser( User user){
        return userService.addUser(user);
    }


    @RequestMapping(value = "/users", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public User addUser(@RequestBody User user){
        System.out.println("(Service Side) Creating user: " + user.getId());
        return userRepository.addUser(user);
    }

    @RequestMapping(value = "/users", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public User updateUser(@RequestBody User user){
        System.out.println("(Service Side) Editing user: " + user.getId());
        return userRepository.updateUser(user);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public void deleteUser(@PathVariable("id") Long id){
        System.out.println("(Service Side) Deleting user: " + id);
        userRepository.deleteUser(id);
    }







}
